package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {

    @Test
    void should_return_a_ticket_when_park_given_parking_lot_and_car() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        // when
        Ticket ticket = parkingLot.park(car);

        // then
        Assertions.assertNotNull(ticket);

    }

    @Test
    void should_return_a_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);
        // when
        Car fetchCar = parkingLot.fetch(ticket);

        // then
        Assertions.assertEquals(car, fetchCar);

    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_two_ticket() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        // when
        Car fetchCar1 = parkingLot.fetch(ticket1);
        Car fetchCar2 = parkingLot.fetch(ticket2);

        // then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);

    }

    @Test
    void should_return_null_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.park(car);
        Ticket wrongTicket = new Ticket();

        // when
        // then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(
                UnrecognizedTicketException.class,
                () -> parkingLot.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());

    }

    @Test
    void should_return_null_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);
        // when
        // then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(
                UnrecognizedTicketException.class,
                () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());

    }

    @Test
    void should_return_null_when_park_given_no_position_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < parkingLot.MAX_CAPACITY; i++) {
            parkingLot.park(new Car());
        }

        Car car11 = new Car();
        // when
        // then
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car11));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());

    }
}
