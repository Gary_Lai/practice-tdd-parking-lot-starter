package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoy {

    private List<ParkingLot> parkingLots = new ArrayList<>();
    public void manageParkingLot(ParkingLot parkingLot) {
        this.parkingLots.add(parkingLot);
    }

    public Ticket park(Car car) {
        final int tmp = 100000000;
        return this.parkingLots.stream()
                .filter((parkingLot) -> !parkingLot.isFull())
                .sorted((a, b) ->
                        (int)(Double.valueOf(b.getSpareParkingSpace()) / b.MAX_CAPACITY
                                - Double.valueOf(a.getSpareParkingSpace()) / a.MAX_CAPACITY) * tmp)
                .findFirst()
                .map(lot->lot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : this.parkingLots) {
            if (parkingLot.isContains(ticket)) return parkingLot.fetch(ticket);
        }
        throw new UnrecognizedTicketException();
    }
}
