##  Daily Summary 

**Time: 2023/7/14**

**Author: 赖世龙**

---

**O (Objective):**    Today, I mainly learned stories, and got a story how to use TDD to develop. Many of today's exercises are very close to actual project development requirements, with constant iterations.

**R (Reflective):**  Busy

**I (Interpretive):**   I wrote a lot of test and update iterations, realizing that the actual development process is not simple

**D (Decisional):**    I will gradually adapt to this TDD, and in the long run, it will save time and be of higher quality

