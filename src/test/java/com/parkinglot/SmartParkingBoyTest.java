package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {

    @Test
    void should_return_ticket_of_parking_lot_2_when_park_given_boy_and_two_parking_lot_and_a_car() {
        // given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(4);
        smartParkingBoy.manageParkingLot(parkingLot1);
        smartParkingBoy.manageParkingLot(parkingLot2);
        Car car = new Car();

        // when
        Ticket ticket = smartParkingBoy.park(car);

        // then
        Assertions.assertNotNull(ticket);
        Assertions.assertTrue(parkingLot2.isContains(ticket));
    }

    @Test
    void should_return_msg_when_fetch_given_parking_lot_and_used_ticket_and_boy() {
        // used ticket
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.manageParkingLot(parkingLot1);
        parkingBoy.manageParkingLot(parkingLot2);
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        // when
        // then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(
                UnrecognizedTicketException.class,
                () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_msg_when_fetch_given_no_position_parking_lot_and_car_and_boy() {
        //given
        Car car11 = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.manageParkingLot(parkingLot1);
        parkingBoy.manageParkingLot(parkingLot2);
        for (int i = 0; i < parkingLot1.MAX_CAPACITY * 2; i++) {
            parkingBoy.park(new Car());
        }

        // when
        // then
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(
                NoAvailablePositionException.class, () -> parkingBoy.park(car11));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

    @Test
    void should_return_two_car_when_fetch_given_parking_lot_and_two_ticket_and_boy() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.manageParkingLot(parkingLot1);
        parkingBoy.manageParkingLot(parkingLot2);
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        // when
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        // then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }

    @Test
    void should_return_msg_when_fetch_given_parking_lot_and_wrong_ticket_and_boy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.manageParkingLot(parkingLot1);
        parkingBoy.manageParkingLot(parkingLot2);
        parkingBoy.park(car);
        Ticket wrongTicket = new Ticket();

        // when
        // then
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(
                UnrecognizedTicketException.class,
                () -> parkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_a_ticket_when_fetch_given_two_parking_lot_and_car_and_boy() {
        //given
        Car car11 = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.manageParkingLot(parkingLot1);
        parkingBoy.manageParkingLot(parkingLot2);
        for (int i = 0; i < parkingLot1.MAX_CAPACITY; i++) {
            parkingLot1.park(new Car());
        }

        // when
        Ticket ticket = parkingBoy.park(car11);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_given_parking_lot_and_ticket_and_boy() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy();
        parkingBoy.manageParkingLot(parkingLot);
        Ticket ticket = parkingBoy.park(car);

        // when
        Car fetchCar = parkingBoy.fetch(ticket);
        // then
        Assertions.assertEquals(car, fetchCar);

    }
}
