package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {


    private Map<Ticket, Car> parkingSpaceMap = new HashMap<>();
    public int MAX_CAPACITY = 10;

    public ParkingLot() {

    }

    public ParkingLot(int capacity) {
        this.MAX_CAPACITY = capacity;
    }

    public Ticket park(Car car) {
        Ticket ticket;
        if (parkingSpaceMap.size() >= MAX_CAPACITY) throw new NoAvailablePositionException();
        ticket = new Ticket();
        parkingSpaceMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        Car car = parkingSpaceMap.get(ticket);
        if (car == null) throw new UnrecognizedTicketException();
        parkingSpaceMap.remove(ticket);
        return car;
    }

    public boolean isFull() {
        return this.MAX_CAPACITY <= this.parkingSpaceMap.size();
    }

    public boolean isContains(Ticket ticket) {
        return parkingSpaceMap.containsKey(ticket);
    }

    public int getSpareParkingSpace () {
        return this.MAX_CAPACITY - this.parkingSpaceMap.size();
    }

}
