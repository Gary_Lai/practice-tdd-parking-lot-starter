package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy {

    private List<ParkingLot> parkingLots = new ArrayList<>();
    public void manageParkingLot(ParkingLot parkingLot) {
        this.parkingLots.add(parkingLot);
    }

    public Ticket park(Car car) {
        return this.parkingLots.stream()
                .filter((parkingLot) -> !parkingLot.isFull())
                .sorted((a, b) -> b.getSpareParkingSpace() - a.getSpareParkingSpace())
                .findFirst()
                .map(lot->lot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : this.parkingLots) {
            if (parkingLot.isContains(ticket)) return parkingLot.fetch(ticket);
        }
        throw new UnrecognizedTicketException();
    }
}
